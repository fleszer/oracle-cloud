Oracle cloud load balancer config 

Conditions
Conditions Match Criteria: If Any Match
Condition Type 1: Request Header "host" Contains "test.flesz.tk"
Action
Route to Backend Set
Backend Set: test

any(http.request.headers[(i 'host')] eq (i 'test.flesz.tk'))